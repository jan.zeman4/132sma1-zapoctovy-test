## Local functions
def copy_to_file(fw, from_file_name, replace_string):
    "Copies file_from_name to file fw, while replacing ___ with replace string"
    fr = open( from_file_name, "r" )
    for line in fr:
        fw.write(line.replace("___", replace_string))
    fr.close 

def generate_assigment(fw, assignmentID, problemIDs ):
    "Generates a single assigment for the given IDs"

    fw.write("%" * 80 + "\n")
    fw.write("\\varianta{" + assignmentID + "}\n")

    for i in range(3):
        copy_to_file(fw, str("src/problem"+ str(i+1) + ".tex"), problemIDs[i])

    return

## Main part of the file
semesterID = "LS~2020/2021" # semester
num_assignments = 8 # number of assigments to generate
variants = list("ABCDEFGHIJ")

# Generate variants 
import random
random.seed

problems = []

for i in range(3):
    random.shuffle( variants )
    problems.append( variants[:] )

# File ids
assignmentID = list("ABCDEFGHIJ")

fw = open( "test.tex", "w" )

copy_to_file(fw, "src/doc_start.tex", "")

fw.write( "\\newcommand{\\mySemester}{" + semesterID + "}\n" )

for i in range( num_assignments ):
    problemIDs = []

    for row in problems:
        problemIDs.extend( row[i] )
    
    generate_assigment(fw, assignmentID[i], problemIDs )

copy_to_file(fw, "src/doc_end.tex", "")

fw.close

## Create new Makefile
print_pages = ""

for i in range(1, num_assignments+1):
    print_pages += " " + str(i) + " " + str(i)
    
fw = open( "Makefile", "w")

copy_to_file(fw, "src/Makefile", print_pages)

## Export each page to a single png file

for i in range(1, num_assignments+1):
    fw.write( "\t pdftoppm -png -gray -cropbox -r 200 -singlefile -f " + str(i) + \
        " -l " + str(i) + " test.pdf varianta_" + assignmentID[i-1] + "\n")

fw.close